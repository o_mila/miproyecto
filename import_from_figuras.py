from figuras import *

print("Aquí va el triángulo: ")
triangulo1 = Triangulo(10, 5, Coordenada(20,30))
print("Coordenada triangulo: ", triangulo1.GetCoordenada())
triangulo1.SetCoordenada(Coordenada(30,70))
print("Nueva coordenada: ", triangulo1.GetCoordenada())

print("Aquí va el cuadrado: ")
cuadrado1 = Cuadrado(40, Coordenada(100,150))
print("Coordenada cuadrado: ", cuadrado1.GetCoordenada())
cuadrado1.SetCoordenada(Coordenada(0,-300))
print("Nueva coordenada: ", cuadrado1.GetCoordenada())

print("Aquí va el circulo: ")
circulo1 = Circulo(40, Coordenada(90,-20))
print("Coordenada circulo: ", circulo1.GetCoordenada())
circulo1.SetCoordenada(Coordenada(0,-300))
print("Nueva coordenada: ", circulo1.GetCoordenada())
