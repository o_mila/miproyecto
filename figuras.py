class Coordenada:
    def __init__(self,x,y):
        self.__x = x
        self.__y = y
    def GetX(self):
        return self.__x
    def GetY(self):
        return self.__y
    def SetX(self,x):
        self.__x = x
    def SetY(self,y):
        self.__y = y

class Cuadrado:
    def __init__(self,lado,coordenada):
        self.__lado = lado
        self.__coordenada = coordenada
    def calcular_superficie(self):
        return self.__lado ** 2
    def GetLado(self):
        return self.__lado
    def SetLado(self, lado):
        self.__lado = lado
    def GetCoordenada(self):
        return self.__coordenada.GetX(), self.__coordenada.GetY()
    def SetCoordenada(self, coordenada):
        self.__coordenada = coordenada

class Triangulo:
    def __init__(self,base,altura,coordenada):
        self.__base=base
        self.__altura=altura
        self.__coordenada = coordenada
    def calcular_superficie(self):
        area=self.__base*self.__altura*0.5
        return area
    def GetBase(self):
        return self.__base
    def GetAltura(self):
        return self.__altura
    def SetBase(self, base):
        self.__base = base
    def SetAltura(self, altura):
        self.__altura = altura
    def GetCoordenada(self):
        return self.__coordenada.GetX(), self.__coordenada.GetY()
    def SetCoordenada(self,coordenada):
        self.__coordenada = coordenada

class Circulo:
    def __init__(self, radio, coordenada):
        self.__radio = radio
        self.__coordenada = coordenada
    def calcular_superficie(self):
        area = pi * self.__radio ** 2
        return area
    def GetRadio(self):
        return self.__radio
    def SetRadio(self, radio):
        self.__radio = radio
    def GetCoordenada(self):
        return self.__coordenada.GetX(), self.__coordenada.GetY()
    def SetCoordenada(self,coordenada):
        self.__coordenada = coordenada
